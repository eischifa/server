/*
Programma che consente di trasformare un NODO in concentratore dati. Viene creato un
access point a cui i vari nodi si collegano inviando i dati acquisiti, questi verranno poi salvati
su una scheda SD
*/
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiClient.h>
#include <SPI.h>
#include <SD.h>



#include <Wire.h>
#include <DS1307.h>
DS1307 clock;

#define __SD_CHIP__ D8
#define __PIN_LED__ D4

//SSID and Password per il tuo Access Point
	const char* __AP_NAME__ = "ALCIS";
	const char* __AP_PASS__ = "password";
	IPAddress __AP_IP__		(192,168,4,22);   //(192,168,4,22);
	IPAddress __AP_GATEWAY__(192,168,4,9);
	IPAddress __AP_SUBNET__	(255,255,255,0);
	
//0=false / 1=true  Permette di abilitare il settaggio del RTC DS1307
	int __SET_TIME__ = 0;			

/*  Parametri per la formattazione della stringa di salvataggio dati */
	String __COMMA__ = ",";
	String __DATE_SEP__= "-";
	String __HOUR_SEP__= ":";
	String __DATE_TIME__,__DATA_ENVIRO__,__IP_REMOTE__,message,__STATUS_STG__;

/*Controllo IP abilitati al salvataggio dati*/
	const int __IP_LIST_ITEMS__ = 7;  
	bool Status = false;  
		String __IP_ALLOWED__[__IP_LIST_ITEMS__] = 
							{ "192.168.4.120", 
							  "192.168.4.121",
							  "192.168.4.122",
							  "192.168.4.123",
							  "192.168.4.124",
							  "192.168.4.125",
							  "192.168.4.126"
							};

ESP8266WebServer server(80); //Server on port 80
 
void handleRoot() {
	__GET_TIME__();
  server.send(200, "text/plain", "ALCIS - TEST - Stoccaggio Dati \nData: " + __DATE_TIME__);
  }

  void handleDownload(){

	File dataFile = SD.open("data.csv");
	  
		if (dataFile) {
			while (dataFile.available()) {
				int fsizeDisk = dataFile.size();
				size_t fsizeSent = server.streamFile(dataFile,"text/plain");
				Serial.print("Lettura File: ");
				Serial.println(fsizeSent);
			}
		dataFile.close();
		} else {
		Serial.println("error opening data.csv");
		}
	  
	  
	  /*
		if (dataFile) {
			while (dataFile.available()) {
				char __FILE_DOWNLOAD__= dataFile.read();
				Serial.print(__FILE_DOWNLOAD__);
				//server.send(200, "text/plain", dataFile.read());
				server.send(200, "text/plain", "Download");
			}
			dataFile.close();
			
		}
		
	  else {
    Serial.println("error opening datalog.txt");
  }
	  */
	  
  }
  
/*Funzione per il salvataggio dati. I dati sono spediti conuna richiesta GET, il link cosi creato
non "esiste" e quindi verrà eseguito il codice  nel "not found"*/  
void handleNotFound() {
	
	__IP_REMOTE__= server.client().remoteIP().toString();
	__GET_TIME__();

	for (int i=0 ; i<__IP_LIST_ITEMS__; i++){
			if (__IP_ALLOWED__[i]==__IP_REMOTE__){
				Status = true;
				__STATUS_STG__= "SI";
				break;
			} else {
				Status = false;
				__STATUS_STG__= "NO";
			} 
		}	

			message = "\nDati: ";
			  message += server.uri();
			  message += "\nIP Client: ";
			  message += __IP_REMOTE__;
			  message += "\nIP abilitato: ";
			  message += __STATUS_STG__;
			  message += "\nStringa: ";
			  message += __DATE_TIME__+ server.uri();

	if (Status == 1){
		

					digitalWrite (__PIN_LED__,LOW);
				File dataFile = SD.open("data.csv", FILE_WRITE);
					if (dataFile){
						dataFile.println(__DATE_TIME__+ server.uri());
						dataFile.close();
					digitalWrite (__PIN_LED__,HIGH);			
					
					} else {
						
					digitalWrite (__PIN_LED__,LOW);
					delay(700);
					digitalWrite (__PIN_LED__,HIGH);
					Serial.print("\nUnable open file!");
					}
		}	  

	server.sendHeader("Connection", "close");
	server.send(200, "text/plain", message);
	Serial.println(message);
		
  }
  

void setup(){
	
  Serial.begin(9600);
  clock.begin();
  
  pinMode (__PIN_LED__,OUTPUT);
  digitalWrite (__PIN_LED__,HIGH);
  
  
  if (__SET_TIME__== 1){
				clock.fillByYMD(2019,10,02);//Jan 19,2013
				clock.fillByHMS(18,19,00);//15:28 30"
				clock.fillDayOfWeek(WED);//Sabato
				clock.setTime();//write time to the RTC chip
				}
  
	
	WiFi.mode(WIFI_AP); 
	WiFi.softAPConfig (__AP_IP__, __AP_GATEWAY__, __AP_SUBNET__);
	WiFi.softAP(__AP_NAME__, __AP_PASS__);  
		Serial.print("HotSpt IP: ");
		Serial.println(WiFi.softAPIP());
 
	server.on("/", handleRoot);
	server.on ("/download",handleDownload);
	server.onNotFound(handleNotFound);	
		
	server.begin(); 
	Serial.println("Server HTTP avviato");
	
	if(!SD.begin(__SD_CHIP__)){
			Serial.println ("error on SD, unable to store data!");
			server.send(200, "text/plain", "Error on SD, unable to store data!");
		}

	__GET_TIME__();
	Serial.println(__DATE_TIME__);
}

void loop(void) {

 server.handleClient();
 
}


void __GET_TIME__(){
	clock.getTime();

  
		__DATE_TIME__=  String(clock.hour, DEC)      + __HOUR_SEP__ +
						String(clock.minute, DEC)    + __HOUR_SEP__ +
						String(clock.second, DEC)    + " "          +
						String(clock.dayOfMonth, DEC)+ __DATE_SEP__ +
						String(clock.month, DEC)     + __DATE_SEP__ +
						String(clock.year+2000, DEC);	

}

/*
void loop()
{
  WiFiClient client = server.available();
  // wait for a client (web browser) to connect
  if (client)
  {
    Serial.println("\n[Client connected]");
		while (client.connected())
			{
			  // read line by line what the client (web browser) is requesting
			  if (client.available())
			  {
				String line = client.readStringUntil('\r');
				Serial.print(line);
				
					if (line.length() == 1 && line[0] == '\n')
					{
					String htmlPage =
						 String("HTTP/1.1 200 OK\r\n") +
								"Content-Type: text/html\r\n" +
								"Connection: close\r\n" +  // the connection will be closed after completion of the response
								"Refresh: 5\r\n" +  // refresh the page automatically every 5 sec
								"\r\n" +
								"<!DOCTYPE HTML>" +
								"<html>" +
								"Analog input:  " + 
								"</html>" +
								"\r\n";
					  client.println(htmlPage);
					  
					  break;
					}
			  }
			}
    delay(1); // give the web browser time to receive the data

    // close the connection:
    client.stop();
    Serial.println("[Client disonnected]");
  }
}

String  test () {
	String htmlPage = "HTTP/1.1 200 OK\r\n";
	return htmlPage;
}

/*
String prepareHtmlPage()
{
  String htmlPage =
     String("HTTP/1.1 200 OK\r\n") +
            "Content-Type: text/html\r\n" +
            "Connection: close\r\n" +  // the connection will be closed after completion of the response
            "Refresh: 5\r\n" +  // refresh the page automatically every 5 sec
            "\r\n" +
            "<!DOCTYPE HTML>" +
            "<html>" +
            "Analog input:  " + 
            "</html>" +
            "\r\n";
  return htmlPage;
}
*/
